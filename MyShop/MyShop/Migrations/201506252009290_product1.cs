namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class product1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "pictureID", "dbo.Pictures");
            DropIndex("dbo.Products", new[] { "pictureID" });
            CreateTable(
                "dbo.Product_Picture_mapping",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        productID = c.Int(nullable: false),
                        pictureID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pictures", t => t.pictureID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.productID, cascadeDelete: true)
                .Index(t => t.productID)
                .Index(t => t.pictureID);
            
            DropColumn("dbo.Products", "pictureID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "pictureID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Product_Picture_mapping", "productID", "dbo.Products");
            DropForeignKey("dbo.Product_Picture_mapping", "pictureID", "dbo.Pictures");
            DropIndex("dbo.Product_Picture_mapping", new[] { "pictureID" });
            DropIndex("dbo.Product_Picture_mapping", new[] { "productID" });
            DropTable("dbo.Product_Picture_mapping");
            CreateIndex("dbo.Products", "pictureID");
            AddForeignKey("dbo.Products", "pictureID", "dbo.Pictures", "id", cascadeDelete: true);
        }
    }
}
