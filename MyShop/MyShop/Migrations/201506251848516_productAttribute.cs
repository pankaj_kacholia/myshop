namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class productAttribute : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductAttributes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        productAttributeOptionsID = c.Int(nullable: false),
                        description = c.String(),
                        categoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ProductAttributeOptions", t => t.productAttributeOptionsID, cascadeDelete: true)
                .Index(t => t.productAttributeOptionsID);
            
            CreateTable(
                "dbo.ProductAttributeOptions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductAttributes", "productAttributeOptionsID", "dbo.ProductAttributeOptions");
            DropIndex("dbo.ProductAttributes", new[] { "productAttributeOptionsID" });
            DropTable("dbo.ProductAttributeOptions");
            DropTable("dbo.ProductAttributes");
        }
    }
}
