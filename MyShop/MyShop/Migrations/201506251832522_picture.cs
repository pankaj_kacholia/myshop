namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class picture : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pictures",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        pictureName = c.String(nullable: false),
                        isRelatedImage = c.Int(nullable: false),
                        mimeType = c.String(nullable: false),
                        seoFileName = c.String(nullable: false),
                        isNew = c.Boolean(nullable: false),
                        displayOrder = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pictures");
        }
    }
}
