namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class product : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        productName = c.String(nullable: false),
                        shortDescription = c.String(nullable: false),
                        fullDescription = c.String(nullable: false),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        oldPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                      //  pictureID = c.Int(nullable: false),
                        categoryID = c.Int(nullable: false),
                        manufucturersID = c.Int(nullable: false),
                        displayStockAvailability = c.Boolean(nullable: false),
                        displayStockQuantity = c.Boolean(nullable: false),
                        isTaxExempt = c.Boolean(nullable: false),
                        taxCategoryId = c.Int(nullable: false),
                        metaKeywords = c.String(),
                        metaDescription = c.String(),
                        metaTitle = c.String(),
                        allowCustomerReviews = c.Boolean(nullable: false),
                        availableStartDisDateTimeUtc = c.String(),
                        availableEndDisDateTimeUtc = c.String(),
                        imagePath = c.String(),
                        sku = c.String(),
                        isGiftCard = c.Boolean(nullable: false),
                        giftCardTypeId = c.Int(nullable: false),
                        isDownload = c.Boolean(nullable: false),
                        downloadId = c.Int(nullable: false),
                        isFreeShippment = c.Boolean(nullable: false),
                        shippingCharge = c.Int(nullable: false),
                        deliveryDateId = c.Int(nullable: false),
                        weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        length = c.Decimal(nullable: false, precision: 18, scale: 2),
                        width = c.Decimal(nullable: false, precision: 18, scale: 2),
                        height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        published = c.Boolean(nullable: false),
                        stockQuantity = c.Int(nullable: false),
                        hasDiscountsApplied = c.Boolean(nullable: false),
                        discounts = c.Decimal(nullable: false, precision: 18, scale: 2),
                        createdOnUtc = c.DateTime(nullable: false),
                        updatedOnUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Categories", t => t.categoryID, cascadeDelete: true)
                .ForeignKey("dbo.Manufucturers", t => t.manufucturersID, cascadeDelete: true)
                //.ForeignKey("dbo.Pictures", t => t.pictureID, cascadeDelete: true)
               // .Index(t => t.pictureID)
                .Index(t => t.categoryID)
                .Index(t => t.manufucturersID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "pictureID", "dbo.Pictures");
            DropForeignKey("dbo.Products", "manufucturersID", "dbo.Manufucturers");
            DropForeignKey("dbo.Products", "categoryID", "dbo.Categories");
            DropIndex("dbo.Products", new[] { "manufucturersID" });
            DropIndex("dbo.Products", new[] { "categoryID" });
            DropIndex("dbo.Products", new[] { "pictureID" });
            DropTable("dbo.Products");
        }
    }
}
