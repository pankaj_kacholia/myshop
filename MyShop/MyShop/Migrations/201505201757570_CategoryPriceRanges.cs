namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryPriceRanges : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "PriceRanges", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categories", "PriceRanges", c => c.String(nullable: false));
        }
    }
}
