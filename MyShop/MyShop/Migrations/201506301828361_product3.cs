namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class product3 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Products", "displayStockAvailability");
            DropColumn("dbo.Products", "displayStockQuantity");
            DropColumn("dbo.Products", "isTaxExempt");
            DropColumn("dbo.Products", "taxCategoryId");
            DropColumn("dbo.Products", "allowCustomerReviews");
            DropColumn("dbo.Products", "availableStartDisDateTimeUtc");
            DropColumn("dbo.Products", "availableEndDisDateTimeUtc");
            DropColumn("dbo.Products", "sku");
            DropColumn("dbo.Products", "isGiftCard");
            DropColumn("dbo.Products", "giftCardTypeId");
            DropColumn("dbo.Products", "isDownload");
            DropColumn("dbo.Products", "downloadId");
            DropColumn("dbo.Products", "isFreeShippment");
            DropColumn("dbo.Products", "shippingCharge");
            DropColumn("dbo.Products", "deliveryDateId");
            DropColumn("dbo.Products", "weight");
            DropColumn("dbo.Products", "length");
            DropColumn("dbo.Products", "width");
            DropColumn("dbo.Products", "height");
            DropColumn("dbo.Products", "stockQuantity");
            DropColumn("dbo.Products", "hasDiscountsApplied");
            DropColumn("dbo.Products", "discounts");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "discounts", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Products", "hasDiscountsApplied", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "stockQuantity", c => c.Int());
            AddColumn("dbo.Products", "height", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Products", "width", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Products", "length", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Products", "weight", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Products", "deliveryDateId", c => c.Int());
            AddColumn("dbo.Products", "shippingCharge", c => c.Int());
            AddColumn("dbo.Products", "isFreeShippment", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "downloadId", c => c.Int());
            AddColumn("dbo.Products", "isDownload", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "giftCardTypeId", c => c.Int());
            AddColumn("dbo.Products", "isGiftCard", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "sku", c => c.String());
            AddColumn("dbo.Products", "availableEndDisDateTimeUtc", c => c.String());
            AddColumn("dbo.Products", "availableStartDisDateTimeUtc", c => c.String());
            AddColumn("dbo.Products", "allowCustomerReviews", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "taxCategoryId", c => c.Int());
            AddColumn("dbo.Products", "isTaxExempt", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "displayStockQuantity", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products", "displayStockAvailability", c => c.Boolean(nullable: false));
        }
    }
}
