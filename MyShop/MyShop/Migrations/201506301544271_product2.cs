namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class product2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "shortDescription", c => c.String());
            AlterColumn("dbo.Products", "fullDescription", c => c.String());
            AlterColumn("dbo.Products", "oldPrice", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Products", "taxCategoryId", c => c.Int());
            AlterColumn("dbo.Products", "giftCardTypeId", c => c.Int());
            AlterColumn("dbo.Products", "downloadId", c => c.Int());
            AlterColumn("dbo.Products", "shippingCharge", c => c.Int());
            AlterColumn("dbo.Products", "deliveryDateId", c => c.Int());
            AlterColumn("dbo.Products", "weight", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Products", "length", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Products", "width", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Products", "height", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Products", "stockQuantity", c => c.Int());
            AlterColumn("dbo.Products", "discounts", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.Products", "imagePath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "imagePath", c => c.String());
            AlterColumn("dbo.Products", "discounts", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "stockQuantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "height", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "width", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "length", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "weight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "deliveryDateId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "shippingCharge", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "downloadId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "giftCardTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "taxCategoryId", c => c.Int(nullable: false));
            AlterColumn("dbo.Products", "oldPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Products", "fullDescription", c => c.String(nullable: false));
            AlterColumn("dbo.Products", "shortDescription", c => c.String(nullable: false));
        }
    }
}
