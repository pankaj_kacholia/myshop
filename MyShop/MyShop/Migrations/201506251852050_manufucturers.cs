namespace MyShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class manufucturers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Manufucturers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        description = c.String(nullable: false),
                        metaKeywords = c.String(nullable: false),
                        metaDescription = c.String(nullable: false),
                        metaTitle = c.String(nullable: false),
                        pictureId = c.Int(nullable: false),
                        published = c.Boolean(nullable: false),
                        isDeleted = c.Boolean(nullable: false),
                        priceRanges = c.String(),
                        displayOrder = c.Int(nullable: false),
                        createdOnUtc = c.DateTime(nullable: false),
                        updatedOnUtc = c.DateTime(nullable: false),
                        categoryid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Manufucturers");
        }
    }
}
