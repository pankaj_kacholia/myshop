﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyShop.Models;

namespace MyShop.Controllers.Admin
{
    public class RolesController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        #region Roles
        public ActionResult Index()
        {
            var roles = db.Roles.ToList();
            return View(roles);
        }

        public ActionResult Create()
        {
            return PartialView("Create");
        }

        [HttpPost]
        public ActionResult Create(string RoleName)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var thisRole = db.Roles.Where(r => r.Name.Equals(RoleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                    if (thisRole == null)
                    {
                        db.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole()
                        {
                            Id = Guid.NewGuid().ToString(),
                            Name = RoleName
                        });
                        db.SaveChanges();
                        ViewBag.ResultMessage = "Role created successfully !";
                    }

                    else
                    {
                        ViewBag.ResultMessage = "Role already exist.";
                    }


                    return Json(new { message = ViewBag.ResultMessage });
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            ViewBag.ResultMessage = "Name can't be empty.";
            return Json(new { message = ViewBag.ResultMessage });
        }

        public ActionResult DeleteRoles(string RoleName)
        {
            var thisRole = db.Roles.Where(r => r.Name.Equals(RoleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            db.Roles.Remove(thisRole);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult EditRoles(string roleName)
        {
            var thisRole = db.Roles.Where(r => r.Name.Equals(roleName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            //return PartialView("EditRoles",thisRole);
            return View(thisRole);
        }

        //
        // POST: /Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRoles(Microsoft.AspNet.Identity.EntityFramework.IdentityRole role)
        {
            try
            {
                db.Entry(role).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                ViewBag.ResultMessage = "Role updated successfully.";
                return Json(new { message = ViewBag.ResultMessage });

            }
            catch
            {
                return View();
            }
        }

        //public ActionResult ManageUserRoles(string sortOrder, string currentFilter, string searchString, int? page)
        //{

        //    ViewBag.CurrentSort = sortOrder;
        //    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
        //    ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            
        //    if (searchString != null)
        //    {
        //        page = 1;
        //    }
        //    else
        //    {
        //        searchString = currentFilter;
        //    }

        //    ViewBag.CurrentFilter = searchString;
        //    List<RoleList> roleobj = new List<RoleList>();
        //    var account = new AccountController();
        //    var user = db1.Users.ToList();
        //    foreach (var userid in user)
        //    {
        //        var data = account.UserManager.GetRoles(userid.Id);
        //        if (data.Count != 0)
        //        {
        //            roleobj.Add(new RoleList { ID = userid.Id, RoleName = data[0], CustomerName = userid.UserName });
        //        }
        //        else
        //        {
        //            roleobj.Add(new RoleList { ID = userid.Id, RoleName = "Not Assign", CustomerName = userid.UserName });
        //        }
        //        //ViewBag.ShowCustomerRole = roleobj;
        //    }

        //    var students = roleobj;
        //    if (!String.IsNullOrEmpty(searchString))
        //    {
        //        students = students.Where(s => s.CustomerName.Contains(searchString)).ToList();

        //    }
        //    switch (sortOrder)
        //    {
        //        case "name_desc":
        //            students = students.OrderByDescending(s => s.CustomerName).ToList();
        //            break;
        //        default:  // Name ascending 
        //            students = students.OrderBy(s => s.CustomerName).ToList();
        //            break;
        //    }

        //    int pageSize = 3;
        //    int pageNumber = (page ?? 1);
        //    return View(students.ToPagedList(pageNumber, pageSize));

        //}
        #endregion

    }
}