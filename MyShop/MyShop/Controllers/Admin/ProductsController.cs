﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyShop;
using MyShop.Models;
using System.IO;
using CommonMethod;

namespace MyShop.Controllers.Admin
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Products
        public ActionResult Index()
        {
            var product = db.product.Include(p => p.category).Include(p => p.manufucturers);
            return View(product.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Product_Picture_mapping> productPictureMapping = db.productPicturemapping.Where(a => a.productID == id).Select(b => b).ToList();
            if (productPictureMapping == null)
            {
                return HttpNotFound();
            }
            return View(productPictureMapping);
        }

        // GET: Products/Create
        public ActionResult Create()
        {

            ViewBag.categoryID = new SelectList(db.Category, "ID", "Name");
            ViewBag.manufucturersID = new SelectList(db.manufucturers, "id", "name");
            return PartialView("Create");
        }

        #region Initialization
		 
        Picture picture = new Picture();
        List<int> pictureId = new List<int>();
        Product_Picture_mapping productPictureMapping = new Product_Picture_mapping();
        #endregion
        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product, HttpPostedFileBase[] images, string PriorImage)
        {
            
            if (ModelState.IsValid)
            {
                product.createdOnUtc = DateTime.Now;
                product.updatedOnUtc = DateTime.Now;
                db.product.Add(product);
                db.SaveChanges();

                if (images.Length != 0 && product.id != 0)
                {
                    Product_Picture_mapping productPictureMapping = new Product_Picture_mapping();
                    picture = AddPicture(images,"Add");

                    productPictureMapping.productID = product.id;
                    foreach (int id in pictureId)
                    {
                        productPictureMapping.pictureID = id;
                        db.productPicturemapping.Add(productPictureMapping);
                        db.SaveChanges();
                    }
                }
                else
                {
                    ViewBag.message = Guideline.ImageAdd;
                }

                return RedirectToAction("Index");
            }

            ViewBag.categoryID = new SelectList(db.Category, "ID", "Name", product.categoryID);
            ViewBag.manufucturersID = new SelectList(db.manufucturers, "id", "name", product.manufucturersID);
            return View(product);
        }

        public Picture AddPicture(HttpPostedFileBase[] images,string status)
        {
            foreach (var image in images)
            {
                string fileName = Path.Combine(Server.MapPath("~/UserImage/"), image.FileName);
                image.SaveAs(fileName);
                picture.pictureName = image.FileName;
                picture.mimeType = image.ContentType;
                picture.seoFileName = "test_seo";
                if(status=="Add")
                db.picture.Add(picture);
                if (status == "Update")
                    db.Entry(picture).State = EntityState.Modified;
                db.SaveChanges();
                pictureId.Add(picture.id);
            }
            return picture;

        }

        public HttpStatusCodeResult BadRequest()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public HttpNotFoundResult ChekResult()
        {
            return HttpNotFound();
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                BadRequest();

            Product_Picture_mapping productPicturemapping = db.productPicturemapping.Where(a => a.product.id == id).Select(y => y).FirstOrDefault();
            productPicturemapping.lstPicture = new List<Picture>();
            productPicturemapping.lstPicture = db.productPicturemapping.Where(a => a.product.id == id).Select(y => y.picture).ToList();
            if (productPicturemapping == null)
                return HttpNotFound();

            ViewBag.categoryID = new SelectList(db.Category, "ID", "Name", productPicturemapping.product.categoryID);
            ViewBag.manufucturersID = new SelectList(db.manufucturers, "id", "name", productPicturemapping.product.manufucturersID);
            return View(productPicturemapping);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product_Picture_mapping productPictureMapping, HttpPostedFileBase[] images)
        {

            if (ModelState.IsValid)
            {
               
                productPictureMapping.product.updatedOnUtc = DateTime.Now;
                db.Entry(productPictureMapping.product).State = EntityState.Modified;
                db.SaveChanges();

                if (images.Length != 0 && productPictureMapping.product.id != 0)
                {

                    picture = AddPicture(images,"Update");

                    productPictureMapping.productID = productPictureMapping.product.id;
                    foreach (int id in pictureId)
                    {
                        productPictureMapping.pictureID = id;
                        db.Entry(productPictureMapping.picture).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    ViewBag.message = Guideline.Update;

                }
                else
                {
                    ViewBag.message = Guideline.ImageAdd;
                }

            }
            ViewBag.categoryID = new SelectList(db.Category, "ID", "Name", productPictureMapping.product.categoryID);
            ViewBag.manufucturersID = new SelectList(db.manufucturers, "id", "name", productPictureMapping.product.manufucturersID);


            return View();
           
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.product.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.product.Find(id);
            db.product.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
