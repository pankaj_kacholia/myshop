﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelLibrary.Category;
using MyShop.Models;
using System.IO;
using System.Data.Entity;
namespace MyShop.Controllers.Admin
{
    public class CategoryController : Controller
    {
        CategoryBindMethod obj = new CategoryBindMethod();
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Category
        public ActionResult Index()
        {   
            return View(db.Category.ToList());
        }

        public ActionResult Create()
        {   
            ViewBag.ParentCategoryId = obj.CategoryBind();
            return PartialView("Create");
        }

        [HttpPost]
        public ActionResult Create(Category Category, HttpPostedFileBase Image)
        {

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            }

            Category.CreatedOnUtc = DateTime.Now;
            Category.UpdatedOnUtc = DateTime.Now;

            if (ModelState.IsValid)
            {
                if (Image != null)
                {
                    Category.PictureName = Image.FileName;
                    Category.PictureType = Image.ContentType;
                }
                if (Category.ParentCategoryId == null)
                    Category.ParentCategoryId = 0;
                
                db.Category.Add(Category);
                db.SaveChanges();
                ViewBag.ResultMessage = "data submitted sucessfully.";
                ModelState.Clear();
                ViewBag.ParentCategoryId = obj.CategoryBind();
                return PartialView("Create");                
            }
            ViewBag.ParentCategoryId = obj.CategoryBind();
            ViewBag.ResultMessage = "Please fill all required fields.";          
            return PartialView("Create");
        }

        public ActionResult EditCategories(int id)
        {
            Category categoryobj = db.Category.Find(id);
        
            List<SelectListItem> CustList = new List<SelectListItem>();
            var Category = db.Category.ToList();
            string CategoryName;
            foreach (var CateId in Category)
            {
                if (CateId.ParentCategoryId != 0)
                {

                    for (int i = 0; i < Category.Count; i++)
                    {
                        if (CateId.ParentCategoryId == Category[i].ID)
                        {
                            CategoryName = Category[i].Name + "<<" + CateId.Name;
                            for (int j = 0; j < Category.Count; j++)
                            {
                                if (Category[i].ParentCategoryId == Category[j].ID)
                                {
                                    CategoryName = Category[j].Name + "<<" + CategoryName;

                                }
                            }
                            if (categoryobj.ID == CateId.ID && CateId.ID != 0)
                            {
                                CustList.Add(new SelectListItem { Text = CategoryName, Value = CateId.ID.ToString(), Selected = true });
                            }
                            else
                            {
                                CustList.Add(new SelectListItem { Text = CategoryName, Value = CateId.ID.ToString() });
                            }
                        }

                    }
                }
                else if (CateId.ParentCategoryId == 0)
                {

                    CategoryName = CateId.Name;
                    if (categoryobj.ID == CateId.ID && CateId.ID != 0)
                    {
                        CustList.Add(new SelectListItem { Text = CategoryName, Value = CateId.ID.ToString(), Selected = true });
                    }
                    else
                    {
                        CustList.Add(new SelectListItem { Text = CategoryName, Value = CateId.ID.ToString() });
                    }
                }
            }



            ViewBag.ParentCategoryId = CustList;

            return PartialView("EditCategories",categoryobj);
        }

        [HttpPost]
        public ActionResult EditCategories(Category cateObj)
        {
            if (ModelState.IsValid)
            {


                if (cateObj.ID != cateObj.ParentCategoryId)
                {
                    cateObj.ParentCategoryId = cateObj.ParentCategoryId;
                }
                else
                {
                    cateObj.ParentCategoryId = db.Category.Where(a => a.ID == cateObj.ParentCategoryId).Select(b => b.ParentCategoryId).FirstOrDefault(); ;
                }
                if (cateObj.ParentCategoryId == null)
                {
                    cateObj.ParentCategoryId = 0;
                }

                db.Entry(cateObj).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.ResultMessage = "data updated sucessfully.";
                ModelState.Clear();
                return Json(new { message = ViewBag.ResultMessage });

               
            }
            ViewBag.ResultMessage = "Please fill all required fields.";
            return Json(new { message = ViewBag.ResultMessage });
            
        }
    }
}