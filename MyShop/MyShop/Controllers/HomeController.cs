﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyShop;
using MyShop.Models;

namespace MyShop.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
        //   Product product= db.product.Where(a => a.id == 8 && a.picture.isRelatedImage==a.id).FirstOrDefault();
         //  product.lstPicture = db.product.Where(a => a.picture.isRelatedImage == a.id).Select(b=>b).FirstOrDefault();
            Product_Picture_mapping productPictureMapping =new Product_Picture_mapping();
            productPictureMapping = db.productPicturemapping.Where(a => a.product.id == 8).Select(y => y).FirstOrDefault();
            productPictureMapping.lstPicture = db.productPicturemapping.Where(a => a.picture.isRelatedImage==8).Select(y => y.picture).ToList();
 
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}