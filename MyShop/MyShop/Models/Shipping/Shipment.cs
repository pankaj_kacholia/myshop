﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Order;
namespace G2MFashions.Models.Shipping
{
    public class Shipment
    {
        public int ID { get; set; }

        [Required]
        public int CustomerOrderID { get; set; }
        public virtual CustomerOrder CustomerOrder { get; set; }

        public decimal TotalWeight { get; set; }

        public string TrackingNumber { get; set; }

        [DataType(DataType.Date)]
        public DateTime ShippedDateUtc { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DeliveryDateUtc { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }



    }
}