﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Shipping
{
    public class ShippingByWeight
    {
        public int ID { get; set; }

        public int StoreID { get; set; }

        public int CountryID { get; set; }

        public int StateID { get; set; }

        public int Quentity { get; set; }

        public decimal AdditionalFixedCost {get;set;}
   
        public decimal RatePerWeightUnit { get; set; }

        public decimal LowerWeightLimit { get; set; }

        public decimal TotalCost { get; set; }


    }
}