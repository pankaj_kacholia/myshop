﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Shipping
{
    public class ShipmentItem
    {
        public int ID { get; set; }


        [Required]
        public int ShipmentID { get; set; }
        public virtual Shipment Shipment { get; set; }

        public int OrderItemID { get; set; }
        public int Quentity { get; set; }
    }
}