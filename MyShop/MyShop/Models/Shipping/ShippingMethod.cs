﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Shipping
{
    public class ShippingMethod
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}