﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.MyAccount;
namespace G2MFashions.Models.Shipping
{
    //each country have  shipping method
    public class ShippingMethodRestrictions
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required]
        public int ShippingMethodID { get; set; }
        public virtual ShippingMethod ShippingMethod { get; set; }

        [Required]
        public int CountryID { get; set; }
        public virtual Country Country { get; set; }
    }
}