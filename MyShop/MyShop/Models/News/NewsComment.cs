﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Customer;
namespace G2MFashions.Models.News
{
    public class NewsComment
    {
        public int ID { get; set; }
        public string CommentTitle { get; set; }
        public string CommentText { get; set; }

        [Required]
        public int NewsId {get;set;}
        public virtual News News { get; set; }

        [Required]
        public int CustomerDetailId { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
    }
}