﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.News
{
    public class NewsLetterSubscription
    {
        public int ID { get; set; }

        public bool Active { get; set; }

        public int StoreId { get; set; }//?

        [Required(ErrorMessage="Can't be empty.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
    }
}