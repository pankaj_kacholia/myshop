﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MyShop
{
    public class Manufucturers
    {

        public int id { get; set; }

        [Required(ErrorMessage = "*")]
        public string name { get; set; }

        [Required(ErrorMessage = "*")]
        public string description { get; set; }

        [Required(ErrorMessage = "*")]
        public string metaKeywords { get; set; }

        [Required(ErrorMessage = "*")]
        public string metaDescription { get; set; }

        [Required(ErrorMessage = "*")]
        public string metaTitle { get; set; }

        public int pictureId { get; set; }

        public bool published { get; set; }

        public bool isDeleted { get; set; }

        public string priceRanges { get; set; }

        public int displayOrder { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime createdOnUtc { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime updatedOnUtc { get; set; }
        public int categoryid { get; set; }
    }
}