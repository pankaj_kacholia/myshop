﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Customer;
using G2MFashions.Models.Product;
using MyShop;
namespace MyShop
{
    public class CustomerWhishList
    {
        //Same as table of ShppingCartItem 
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        //[Required]
        //public int StoreID { get; set; }
        //public virtual Store Store { get; set; }

        [Required]
        public int CustomerDetailID { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }

        [Required]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        //public Virtual Vender VenderId{get;set;}

        [Required]
        public int ManufucturersID { get; set; }
        public virtual Manufucturers Manufucturers { get; set; }


        [Required(ErrorMessage = "Quentity can not be empty.")]
        public int Quentity { get; set; }

        public decimal ProductPrice { get; set; }

        private decimal ProductDiscountExclPrice { get; set; }

        private decimal ProductDiscountedInclPrice { get; set; }

        public decimal ProductDiscount { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpectedDeliveryDate { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime UpdatedOnUtc { get; set; }

    }
}