﻿using G2MFashions.Models.Order;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShop
{
    public class PaymentAdmintoVendor
    {
        public int ID { get; set; }

        //[Required]
        //public int OrderItemID { get; set; }
        //public virtual OrderItem OrderItem { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime DateAdd { get; set; }

        public DateTime DateUpdate { get; set; }

        public decimal PaymentRs { get; set; }

        public bool Ispayment { get; set; }

        public decimal PaymentPercentage { get; set; }

        public decimal PaymentDiscount { get; set; }
    }
}
