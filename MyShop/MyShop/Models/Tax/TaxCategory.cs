﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2MFashions.Models.Tax
{
    public class TaxCategory
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }
}