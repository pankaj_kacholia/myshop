﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2MFashions.Models.Tax
{
    public class TaxRate
    {
        public int ID { get; set; }
        public int StoreId { get; set; }
        public int TaxCategoryId { get; set; }
        public int CountryId { get; set; }
        public int Percentage { get; set; }
    }
}