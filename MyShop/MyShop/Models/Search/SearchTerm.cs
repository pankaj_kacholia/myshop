﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Search
{
    public class SearchTerm
    {
        public int ID { get; set; }
        public string Keyword { get; set; }
        public int Count { get; set; }
    }
}