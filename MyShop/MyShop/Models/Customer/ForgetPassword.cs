﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Customer
{
    public class ForgetPassword
    {

        [Required(ErrorMessage="*")]
        public string Email { get; set; }

    }
}