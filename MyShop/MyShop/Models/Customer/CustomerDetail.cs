﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace G2MFashions.Models.Customer
{


   
        public class CustomerDetail
        {
            [ScaffoldColumn(false)]
            public int ID { get; set; }
           
            public string CustomerID { get; set; }
            public string CustomerName { get; set; }

            public string Email { get; set; }
          
            [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
            public DateTime CreatedOnUtc { get; set; }

            [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
            public DateTime LastLoginDateUtc { get; set; }

            public double LastLoginTotalTime { get; set; }

            public string LastIpAddress { get; set; }

            [DefaultValue(false)]
            public bool IsDeleted { get; set; }

            [DefaultValue(true)]
            public bool IsActive { get; set; }


         

        }
    
}