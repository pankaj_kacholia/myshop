﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Customer
{
    public class LoginViewModel
    {
     
            [Required(ErrorMessage="*")]
            [Display(Name = "User name")]
            public string UserName { get; set; }

            [Required(ErrorMessage = "*")]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
       
    }
}