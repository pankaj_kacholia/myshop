﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyShop;
namespace MyShop
{
    class CategoryMapping
    {
        // have to  mapping to product
        // have to mapping with image
        //have to mapping with brand
        //have to mapping with seller
        //have to mapping with review for vendor/product

        //Note: one category have only one product 
        // a product have multiple image,
        // a product have multiple vendor,
        // a product have only brand
        // a product have multiple attribute(product filter title)
        // a attribute have multiple value(product filter value)

        // a product have mulptiple review and a vendor have mulptiple review...


        [Required]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        [Required]
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }
    }
}
