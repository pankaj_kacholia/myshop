﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MyShop;
namespace MyShop
{
    public class Category
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required(ErrorMessage = "*")]
        public string Name { get; set; }

        public int? ParentCategoryId { get; set; }

        public string PictureName { get; set; }

        public string PictureType { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        [Required(ErrorMessage = "*")]
        public string MetaKeyword { get; set; }

        [Required(ErrorMessage = "*")]
        public string MetaDescription { get; set; }

        [Required(ErrorMessage = "*")]
        public string MetaTitle { get; set; }

        //[Required(ErrorMessage = "*")]
        public string PriceRanges { get; set; }

        public bool ShowOnHomePage { get; set; }

        public bool IncludeInTopMenu { get; set; }

        public bool Published { get; set; }

        public bool IsDeleted { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime UpdatedOnUtc { get; set; }

        //public IList<ProductPicture> ProductPicture { get; set; }

    }
}