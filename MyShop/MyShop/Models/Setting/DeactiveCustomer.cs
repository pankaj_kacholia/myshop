﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace BL.Models.Setting
{
  public  class DeactiveCustomer
    {

        [DataType(DataType.Date)]
        public string Email { get; set; }


        [Required(ErrorMessage = "Can't be empty.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
