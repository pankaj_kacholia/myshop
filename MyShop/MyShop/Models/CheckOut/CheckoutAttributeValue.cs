﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.CheckOut
{
    public class CheckoutAttributeValue
    {
        public int ID { get; set; }

        [Required]
        public int CheckoutAttributeID { get; set; }
        public virtual CheckoutAttribute CheckoutAttribute { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int DisplayOrder { get; set; }
    }
}