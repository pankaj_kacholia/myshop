﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models;
using G2MFashions.Models.Customer;
namespace G2MFashions.Models.Log
{
    public class Log
    {
        public int ID { get; set; }

        [Required]  
        public int CustomerDetailID { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }

        public string ShortMessage { get; set; }
        public string FullMessage { get; set; }
        public string IpAddress { get; set; }
     
        public string Url { get; set; }

        public string ReferrerUrl { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
      

    }
}