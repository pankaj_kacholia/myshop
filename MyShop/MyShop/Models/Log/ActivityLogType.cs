﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Log
{
    public class ActivityLogType
    {
        public int ID { get; set; }
        public string SystemKeyword { get; set; }
        public string Name { get; set; }
        public bool Enable { get;set; }
    }
}