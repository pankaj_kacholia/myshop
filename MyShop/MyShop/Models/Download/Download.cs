﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Download
{
    public class Download
    {
        public int ID { get; set; }

        public string Download_File_Name { get; set; }

        public string ContentType { get; set; }

        public string Extension { get; set; }

        public string CustomerID { get; set; }

        public int Count { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
    }
}