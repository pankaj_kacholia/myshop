﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Models.Vendor;
using G2MFashions.Models.Customer;
using MyShop;
namespace MyShop
{
   public class Vendorrating
    {
       public int ID { get; set; }

       [Required]
       public int VendorID { get; set; }
       public virtual Vendor Vendor { get; set; }

       [Required]
       public int CustomerDetailID { get; set; }
       public virtual CustomerDetail CustomerDetail { get; set; }

       public string Rating { get; set; }

       public string Comment { get; set; }

       public DateTime DateAdd { get; set; }

       public DateTime DateModify { get; set; }


    }
}
