﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MyShop
{
    public class Picture
    {
        [ScaffoldColumn(false)]
        public int id { get; set; }

        [Required(ErrorMessage="*")]
        public string pictureName { get; set; }

        [Required]
        public int isRelatedImage { get; set; }

        [Required(ErrorMessage = "*")]
        public string mimeType { get; set; }

        [Required(ErrorMessage = "*")]
        public string seoFileName { get; set; }

        public bool isNew { get; set; }

        public bool displayOrder { get; set; }

    }
}