﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MyShop;

namespace MyShop
{
    public class Product
    {

        public int id { get; set; }
       
        [Required(ErrorMessage = "*")]
        public string productName { get; set; }

        public string shortDescription { get; set; }

        public string fullDescription { get; set; }

        [Required(ErrorMessage = "*")]
        public decimal price { get; set; }

        public decimal? oldPrice { get; set; }

        // added this field later

        //[Required]
        //public int vendorID { get; set; }
        //public virtual Vendor vendor { get; set; }

        //[Required]
        //public int pictureID { get; set; }
        //public virtual Picture picture { get; set; }

        //public List<Picture> lstPicture = new List<Picture>();

        [Required]
        public int categoryID { get; set; }
        public virtual Category category { get; set; }

        [Required]
        public int manufucturersID { get; set; }
        public virtual Manufucturers manufucturers { get; set; }

        //public bool displayStockAvailability { get; set; }

        //public bool displayStockQuantity { get; set; }

        //public bool isTaxExempt { get; set; }

        //public int? taxCategoryId { get; set; }

        public string metaKeywords { get; set; }

        public string metaDescription { get; set; }

        public string metaTitle { get; set; }

        //public bool allowCustomerReviews { get; set; }

        //public string availableStartDisDateTimeUtc { get; set; }

        //public string availableEndDisDateTimeUtc { get; set; }

        //public string sku { get; set; }

        //public bool isGiftCard { get; set; }

        //public int? giftCardTypeId { get; set; }

        //public bool isDownload { get; set; }

        //public int? downloadId { get; set; }

        //public bool isFreeShippment { get; set; }

        //public int? shippingCharge { get; set; }

        //public int? deliveryDateId { get; set; }

        //public decimal? weight { get; set; }

        //public decimal? length { get; set; }

        //public decimal? width { get; set; }

        //public decimal? height { get; set; }

        public bool published { get; set; }

        //public int? stockQuantity { get; set; }

        //public bool hasDiscountsApplied { get; set; }

        //public decimal? discounts { get; set; }

        [DataType(DataType.Date)]
        public DateTime createdOnUtc { get; set; }

        [DataType(DataType.Date)]
        public DateTime updatedOnUtc { get; set; }



    }
}