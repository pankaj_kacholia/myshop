﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
//using G2MFashions.Models.Manufacturers;
using MyShop;
namespace MyShop
{
    public class Product_Manufacturer_Mapping
    {
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        [Required]
        public int ManufucturersID { get; set; }
        public virtual Manufucturers Manufucturers { get; set; }
    }
}