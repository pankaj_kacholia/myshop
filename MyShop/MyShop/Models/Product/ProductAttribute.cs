﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MyShop;

namespace MyShop
{
    public class ProductAttribute
    {
        public int id { get; set; }

        [Required(ErrorMessage = "*")]
        public string name { get; set; }

        [Required]
        public int productAttributeOptionsID { get; set; }
        public virtual ProductAttributeOptions productAttributeOptions { get; set; }

        public string description { get; set; }

        public int categoryId { get; set; }
    }
}