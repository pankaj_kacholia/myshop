﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MyShop
{
    public class ProductVariantAttributeValue
    {
        public int ID { get; set; }

        [Required]
        public int Product_ProductAttribute_MappingID { get; set; }
        public virtual Product_ProductAttribute_Mapping Product_ProductAttribute_Mapping { get; set; }

        public string Name { get; set; }

        public decimal PriceAdjustment { get; set; }

        public int Quentity { get; set; }

        public int PictureId { get; set; }
    }
}