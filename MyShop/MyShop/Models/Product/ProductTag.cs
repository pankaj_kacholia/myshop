﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2MFashions.Models.Product
{
    public class ProductTag
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}