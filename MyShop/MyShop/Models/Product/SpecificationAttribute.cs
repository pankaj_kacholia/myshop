﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Product
{
    public class SpecificationAttribute
    {
        public int ID { get; set; }

        [Required(ErrorMessage="*")]
        public string Name { get; set; }

        public int CategoryId { get; set; }
       // public ICollection<SpecificationAttributeOption> SpecificationAttributeOption { get; set; }
    }
}