﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Product
{
    public class SpecificationAttributeOption
    {
        public int ID { get; set; }

        [Required]
        public int SpecificationAttributeID { get; set; }
        public virtual SpecificationAttribute SpecificationAttribute { get; set; }

        [Required(ErrorMessage="*")]
        public string Name { get; set; }
    }
}