﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MyShop;
namespace MyShop
{
    public class ProductReview
    {
        public int ID { get; set; }

        [Required]
        public int productID { get; set; }
        public virtual Product product { get; set; }
         
        //[Required]
        //public int CustomerDetailID { get; set; }
        //public virtual CustomerDetail CustomerDetail { get; set; }

        public bool IsApproved { get; set; }

        [Required(ErrorMessage = "*")]
        public string Title { get; set; }

        [Required(ErrorMessage="*")]
        public string ReviewText { get; set; }

       //[Required(ErrorMessage = "*")]
        public decimal Rating { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }

       
    }
}