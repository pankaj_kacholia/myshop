﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ModelLibrary.Category;
namespace MyShop
{
    public class Product_Category_Mapping
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }
        public virtual Product CustomerProduct { get; set; }

        [Required]
        public int CustomerCategoryID { get; set; }
        public virtual Category CustomerCategory { get; set; }

        public bool IsFeaturedProduct { get; set; }
    }
}