﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MyShop
{
    public class Product_Picture_mapping
    {
        [ScaffoldColumn(false)]
        public int id { get; set; }

        [Required]
        public int productID { get; set; }
        public virtual Product product { get; set; }

        [Required]
        public int pictureID { get; set; }
        public virtual Picture picture { get; set; }

        public  List<Picture> lstPicture = new List<Picture>();
        
    }
}