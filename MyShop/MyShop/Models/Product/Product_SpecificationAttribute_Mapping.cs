﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using MyShop;
using System.ComponentModel.DataAnnotations;
namespace MyShop
{
    public class Product_SpecificationAttribute_Mapping
    {

        public int ID { get; set; }

        [Required]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        //[Required]
        //public int SpecificationAttributeID { get; set; }
        //public virtual SpecificationAttribute SpecificationAttribute { get; set; }

        public int SpecificationAttributeoptionid { get; set; }

        public bool AllowFiltering { get; set; }
                
        public bool ShowOnProductPage { get; set; }

    }
}