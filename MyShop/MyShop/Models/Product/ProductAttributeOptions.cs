﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MyShop
{
    public class ProductAttributeOptions
    {
        public int id { get; set; }

        [Required(ErrorMessage = "*")]
        public string name { get; set; }
    }
}