﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.ReturnRequest
{
    public class ReturnRequest
    {
        public int ID { get; set; }
        public int StoreId { get; set; }

        public int OrderItemId { get; set; }
        public int CustomerId { get; set; }
        public int Quantity { get; set; }
        public string ReasonForReturn { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReturnDate { get; set; }
        
    }
}