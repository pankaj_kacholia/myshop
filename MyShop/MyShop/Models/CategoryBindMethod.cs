﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelLibrary.Category;
namespace MyShop.Models
{
    public class CategoryBindMethod
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public List<SelectListItem> CategoryBind()
        {

            List<SelectListItem> CustList = new List<SelectListItem>();
            var Category = db.Category.ToList();
            string CategoryName;
            foreach (var CateId in Category)
            {
                if (CateId.ParentCategoryId != 0)
                {

                    for (int i = 0; i < Category.Count; i++)
                    {
                        if (CateId.ParentCategoryId == Category[i].ID)
                        {
                            CategoryName = Category[i].Name + "<<" + CateId.Name;
                            for (int j = 0; j < Category.Count; j++)
                            {
                                if (Category[i].ParentCategoryId == Category[j].ID)
                                {
                                    CategoryName = Category[j].Name + "<<" + CategoryName;

                                }
                            }
                            CustList.Add(new SelectListItem { Text = CategoryName, Value = CateId.ID.ToString() });
                        }

                    }
                }
                else if (CateId.ParentCategoryId == 0)
                {
                    CategoryName = CateId.Name;
                    CustList.Add(new SelectListItem { Text = CategoryName, Value = CateId.ID.ToString() });

                }
            }

            return CustList;
        }

    }
}