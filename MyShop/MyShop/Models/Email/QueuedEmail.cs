﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Email
{
    public class QueuedEmail
    {
        public int ID { get; set; }

        [Required]
        public int EmailAccountID{get;set;}
        public virtual EmailAccount EmailAccount { get; set; }

        public int Priority { get; set; }

        [DataType(DataType.EmailAddress)]
        public string From { get; set; }

        public string FromName { get; set; }


        [DataType(DataType.EmailAddress)]
        public string To { get; set; }

        public string ToName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string AttachmentFilePath { get; set; }
        public string AttachmentFileName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
      
    }
}