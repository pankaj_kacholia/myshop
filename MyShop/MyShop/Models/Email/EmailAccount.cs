﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Email
{
    public class EmailAccount
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Enter your email address.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public int Priority { get; set; }//?

        [Required(ErrorMessage = "Enter your host name.")]
        public string Host { get; set; }

        [Required(ErrorMessage = "Enter your port no.")]
        public string Port { get; set; }

        [Required(ErrorMessage = "Enter your user name.")]
        public string UserName { get; set; }


        [Required(ErrorMessage = "Enter your Password.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool EnableSsl { get; set; }

        public bool UseDefaultCredentials { get; set; }
    }
}