﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.MyAccount
{
    public class CustomerAddress
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }


        [Required(ErrorMessage = "Please enter your name.")]
        [Display(Name = "First Name")]
        public string UserName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }


        [Required(ErrorMessage = "Please enter your email address.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Required(ErrorMessage = "Please enter your contact no.")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Contact No")]
        [StringLength(10, ErrorMessage = "Enter only 10 digit no.")]
        public string ContactNo { get; set; }

        public bool NewsLetter { get; set; }

        [Required(ErrorMessage = "Select your gender")]
        public string Gender { get; set; }


        [DataType(DataType.Date)]
        public DateTime CreatedOnUtc { get; set; }


        [Required(ErrorMessage = "Enter your postal code.")]
        [DataType(DataType.PostalCode)]
        [Display(Name = " Zip / postal code")]
        public string PostalCode { get; set; }


        [Display(Name = "Fax no.")]
        public string FaxNo { get; set; }


        [Required(ErrorMessage="Select your country.")]
        public int CountryID { get; set; }


        [Required(ErrorMessage = "Select your state.")]
         public int StateID { get; set; }


        [Required(ErrorMessage = "Enter your city.")]
        public string City { get; set; }


        [Required(ErrorMessage="Enter your address.")]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        [DataType(DataType.Url)]
        public string Website { get; set; }

        public string CompanyName { get; set; }
       

    }
}