﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.MyAccount
{
    public class Country
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required(ErrorMessage="Enter your country name.")]
        public string CountryName { get; set; }
        public bool AllowBilling { get; set; }
        public bool AllowShipping { get; set; }
        public bool Published { get; set; }

     

    }
}