﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Customer;
namespace G2MFashions.Models.MyAccount
{
    public class CustomerAddresses_Mapping
    {
        public int ID { get; set; }

        [Required]
        public int CustomerAddressID { get; set; }
        public virtual CustomerAddress CustomerAddress { get; set; }


        [Required]
        public int CustomerDetailID { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }
    }
}