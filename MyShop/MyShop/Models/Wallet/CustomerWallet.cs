﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Wallet
{
    public class CustomerWallet
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }
        public  int CustomerId { get; set; }
        public decimal AvailableBalance { get; set; }
 
       
    }
}