﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Customer;
namespace G2MFashions.Models.Blogs
{
    public class BlogComment
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required]
        public int CustomerDetailId { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }

        [Required]
        public int BlogPostID { get; set; }
        public virtual BlogPost BlogPost { get; set; }


        [Required(ErrorMessage="Comment can't be empty.")]
        public string CommentText { get;set;}

        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
      
    }
}