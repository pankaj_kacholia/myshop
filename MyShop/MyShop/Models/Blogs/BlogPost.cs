﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Blogs
{
    public class BlogPost
    {
        public int ID { get; set; }
        public string Title { get; set; }

        public string Body { get; set; }
        public bool AllowComments { get; set; }
        public int CommentCount { get; set; }
        public string Tag { get; set; }
        public string MetaKeywords { get; set; }

        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }

    }
}