﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ModelLibrary.Category;
namespace G2MFashions.Models.Store
{
    public class Store
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string StoreName { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string StoreOwnerName { get; set; }


        public string Country { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string StoreAddress { get; set; }


        public string State { get; set; }

        public string City { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedOnUtc { get; set; }


        public int VenderId { get; set; }

        public bool Active { get; set; }
    }
}