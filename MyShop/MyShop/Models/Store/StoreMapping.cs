﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MyShop;
namespace MyShop
{
    public class StoreMapping
    {
        public int ID { get; set; }


        //[Required]
        //public int StoreID { get; set; }
        //public virtual Store Store { get; set; }

       
        [Required]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }

        public string ProductName { get; set; }
    }
}