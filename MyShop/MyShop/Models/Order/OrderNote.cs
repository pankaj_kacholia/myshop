﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Order
{
    public class OrderNote
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required]
        public int CustomerOrderId { get; set; }
        public virtual CustomerOrder CustomerOrder { get; set; }

        public string Note { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
    }
}