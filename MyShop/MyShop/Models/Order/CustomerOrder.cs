﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Customer;
using G2MFashions.Models.MyAccount;
namespace G2MFashions.Models.Order
{
    public class CustomerOrder
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }


        [Required]
        public int CustomerDetailID{get;set;}
        public virtual CustomerDetail CustomerDetail { get; set; }

        [Required]
        public int CustomerAddressShippingID {get;set;}
        public virtual CustomerAddress CustomerAddress_Shipping { get; set; }


        [Required]
        public int CustomerAddressBillingAddressID { get; set; }
        public CustomerAddress CustomerAddress_Billing_Address { get; set; }

        public bool PickUpInStore{get;set;}

        public int OrderStatusId{get;set;}//?

        public int ShippingStatusId{get;set;}//?

        public int StoreId{get;set;}

        public bool IsDeleted { get; set;}

        public string PaymentMethod { get; set;}    
        
        public string OrderTotal { get; set; }

        public string CheckOutAttributeDescription { get; set; }

        public string CustomerIp { get; set; }

        public string CardType { get; set; }

        public string CardName { get; set; }

        public string CardNumber { get; set; }

        public string CardCvv { get; set; }

        public string CardExpirationMonth { get; set; }

        public string CardExpirationYear { get; set; }

        public decimal TaxRate { get; set; }
        
        public decimal OrderTax { get; set; }
        public decimal OrderDiscount { get; set; }
        public decimal Payment { get; set; }

        public string RefundAmount { get; set; }

        public string ShippingMethod { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }

        
        }

    }
