﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
//using G2MFashions.Models.Product;
//using G2MFashions.Models.Manufacturers;
using MyShop;
//using BL.Models.Vendor;
namespace MyShop
{
    public class OrderItem
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        //[Required]
        //public int CustomerOrderID { get; set; }
        //public virtual CustomerOrder CustomerOrder { get; set; }

        [Required]
        public int ProductID { get;set;}
        public virtual Product  Product { get; set; }

        [Required]
        public int ManufucturersID { get; set; }
        public virtual Manufucturers Manufucturers { get; set; }

        [Required]
        public int VendorID { get; set; }
        public virtual Vendor  Vendor { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPriceInclTax { get; set; }

        public decimal UnitPriceExclTax { get; set; }

        public decimal PriceInclTax { get; set; }

        public decimal ItemWeight { get; set; }

        public int DownloadCount { get; set; }//?

        public int IsDownloadActivated { get; set; }//?

        public decimal OriginalProductCost { get; set; }

        public string Status { get; set; }

        public bool orderstatuscomplete { get; set; }

        public string Processdate { get; set; }
        public string Completedate { get; set; }
        public string Canceldate { get; set; }

        public string Returndate { get; set; }

        public bool Payment { get; set; }
    }
}