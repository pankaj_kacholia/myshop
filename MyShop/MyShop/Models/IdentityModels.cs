﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyShop;
namespace MyShop.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        //#region Blogs
        //public DbSet<Blogs.BlogPost> BlogPost { get; set; }
        //public DbSet<Blogs.BlogComment> BlogComment { get; set; }
        //#endregion

        #region Category
        public DbSet<Category> Category { get; set; }
        #endregion

        //#region CheckOut  
        //public DbSet<CheckOut.CheckoutAttribute> CheckoutAttribute { get; set; }
        //public DbSet<CheckOut.CheckoutAttributeValue> CheckoutAttributeValue { get; set; }
        //#endregion

        //#region Customer
        //public DbSet<Customer.CustomerDetail> CustomerDetail { get; set; }
        //#endregion

        //#region DeliveryDate
        //public DbSet<DeliveryDate.DeliveryDate> DeliveryDate { get; set; }
        //#endregion

        //#region Discount
        //public DbSet<Discount.Discount> Discount { get; set; }
        //public DbSet<Discount.Discount_AppliedToCategories> Discount_AppliedToCategories { get; set; }
        //public DbSet<Discount.Discount_AppliedToProducts> Discount_AppliedToProducts { get; set; }
        //public DbSet<Discount.DiscountUsageHistory> DiscountUsageHistory { get; set; }
        //#endregion

        //#region Download
        //public DbSet<Download.Download> Download { get; set; }
        //#endregion

        //#region Email
        //public DbSet<Email.EmailAccount> EmailAccount { get; set; }
        //public DbSet<Email.QueuedEmail> QueuedEmail { get; set; }
        //#endregion

        //#region Log
        //public DbSet<Log.Log> Log { get; set; }
        //public DbSet<Log.ActivityLog> ActivityLog { get; set; }
        //public DbSet<Log.ActivityLogType> ActivityLogType { get; set; }
        //#endregion

        #region Manufacturers
        public DbSet<Manufucturers> manufucturers { get; set; }
        #endregion

        //#region MyAccount
        //public DbSet<MyAccount.Country> Country { get; set; }
        //public DbSet<MyAccount.CustomerAddresses_Mapping> CustomerAddresses_Mapping { get; set; }
        //public DbSet<MyAccount.CustomerAddress> CustomerAddress { get; set; }
        //public DbSet<MyAccount.State> State { get; set; }
        //public DbSet<BL.Models.MyAccount.City> City { get; set; }
        //#endregion

        //#region News
        //public DbSet<News.News> News { get; set; }
        //public DbSet<News.NewsComment> NewsComment { get; set; }

        //public DbSet<News.NewsLetterSubscription> NewsLetterSubscription { get; set; }
        //#endregion

        //#region Order
        //public DbSet<Order.CustomerOrder> CustomerOrder { get; set; }
        //public DbSet<Order.OrderItem> OrderItem { get; set; }
        //public DbSet<Order.OrderNote> OrderNote { get; set; }

        //#endregion

        #region Picture
        public DbSet<Picture> picture{ get; set; }
        #endregion

        //#region Poll
        //public DbSet<Poll.Poll> Poll { get; set; }
        //public DbSet<Poll.PollAnswer> PollAnswer { get; set; }
        //public DbSet<Poll.PollVotingRecord> PollVotingRecord { get; set; }

        //#endregion

        #region Product
        public DbSet<Product> product { get; set; }
        public DbSet<ProductAttribute> productAttribute { get; set; }
        public DbSet<ProductAttributeOptions> productAttributeOptions { get; set; }
        public DbSet<Product_Picture_mapping> productPicturemapping { get; set; }
        //public DbSet<Product.Product_SpecificationAttribute_Mapping> Product_SpecificationAttribute_Mapping { get; set; }
        //public DbSet<Product.ProductAttribute> ProductAttribute { get; set; }
        //public DbSet<Product.ProductReview> ProductReview { get; set; }
        //public DbSet<Product.ProductTag> ProductTag { get; set; }
        //public DbSet<Product.SpecificationAttribute> SpecificationAttribute { get; set; }
        //public DbSet<Product.SpecificationAttributeOption> SpecificationAttributeOption { get; set; }
        //public DbSet<Product.ProductVariantAttributeValue> ProductVariantAttributeValue { get; set; }
        #endregion

        //#region ReturnRequest
        //public DbSet<ReturnRequest.ReturnRequest> ReturnRequest { get; set; }
        //#endregion

        //#region ScheduleTask
        //public DbSet<ScheduleTask.ScheduleTask> ScheduleTask { get; set; }
        //#endregion

        //#region Search
        //public DbSet<Search.SearchTerm> SearchTerm { get; set; }
        //#endregion

        //#region Shipping
        //public DbSet<Shipping.Shipment> Shipment { get; set; }
        //public DbSet<Shipping.ShipmentItem> ShipmentItem { get; set; }
        //public DbSet<Shipping.ShippingByWeight> ShippingByWeight { get; set; }
        //public DbSet<Shipping.ShippingMethod> ShippingMethod { get; set; }
        //public DbSet<Shipping.ShippingMethodRestrictions> ShippingMethodRestrictions { get; set; }
        //#endregion

        //#region ShoppingCart
        //public DbSet<ShoppingCart.ShoppingCartItem> ShoppingCartItem { get; set; }
        //#endregion

        //#region Store
        //public DbSet<Store.Store> Store { get; set; }
        //public DbSet<Store.StoreMapping> StoreMapping { get; set; }
        //#endregion

        //#region Tax
        //public DbSet<Tax.TaxCategory> TaxCategory { get; set; }
        //public DbSet<Tax.TaxRate> TaxRate { get; set; }
        //#endregion

        //#region Vendor
        //public DbSet<VendorRegistration> VendorRegistration { get; set; }
        //public DbSet<VendorCategoryMapping> VendorCategoryMapping { get; set; }
        //public DbSet<VendorContactVarification> VendorContactVarification { get; set; }
        //#endregion

        //#region Wallet
        //public DbSet<Wallet.CustomerWallet> CustomerWallet { get; set; }
        //#endregion

        //#region WhishList

        //public DbSet<WhishList.CustomerWhishList> CustomerWhishList { get; set; }

        //#endregion

        //#region VendorRating
        //public DbSet<Vendorrating> Vendorrating { get; set; }

        //#endregion

        //#region GiftCard
        //public DbSet<GiftCard> GiftCard { get; set; }

        //#endregion

        //#region Payment
        //public DbSet<PaymentAdmintoVendor> PaymentAdmintoVendor { get; set; }

        //#endregion
    }
}