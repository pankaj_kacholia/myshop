﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.ScheduleTask
{
    public class ScheduleTask
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Seconds { get; set; }

        public bool Enable { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LastSuccessUtc { get; set; }

    }
}