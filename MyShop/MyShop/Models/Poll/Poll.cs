﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2MFashions.Models.Poll
{
    public class Poll
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool ShowOnHomePage { get; set; }
        public bool AllowGuestsToVote { get; set; }
    }
}