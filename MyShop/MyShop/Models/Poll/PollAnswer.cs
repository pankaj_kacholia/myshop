﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace G2MFashions.Models.Poll
{
    public class PollAnswer
    {
        public int ID { get; set; }

        [Required]
        public int PollID { get; set; }
        public virtual Poll Poll { get; set; }

        public int NumberOfVotes { get; set; }
    }
}