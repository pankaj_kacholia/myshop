﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Customer;
namespace G2MFashions.Models.Poll
{
    public class PollVotingRecord
    {
        public int ID { get; set; }

        [Required]
        public int PollAnswerID { get; set; }
        public virtual PollAnswer PollAnswer { get; set; }


        [Required]
        public int CustomerDetailID { get; set; }
        public virtual CustomerDetail CustomerDetail { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
    }
}