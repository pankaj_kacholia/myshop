﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ModelLibrary.Category;
namespace BL.Models.Vendor
{
   public class VendorCategoryMapping
    {
        public int ID { get; set; }

        [Required]
        public int VendorID { get; set; }
        // public int VendorRegistrationID { get; set; }
        // public virtual VendorRegistration VendorRegistration { get; set; }


        [Required]
        public int CategoryID { get; set; }
        // public int CustomerCategoryID { get; set; }
        // public virtual CustomerCategory CustomerCategory { get; set; }
    }
}
