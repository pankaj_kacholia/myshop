﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.MyAccount;
using ModelLibrary.Category;
using G2MFashions.Models.Store;
using G2MFashions.Models;
namespace BL.Models.Vendor
{
    public class VendorContactVarification
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Please enter Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter StoreName")]
        public string StoreName { get; set; }
        [Required(ErrorMessage = "Please enter Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Pin code.")]
        public int Pincode { get; set; }

        public string Country { get; set; }

        public int State { get; set; }

        public string City { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string PickUpAddress { get; set; }


        //VendorBussinessDetail


        public string BussniessName { get; set; }


        [Required(ErrorMessage = "Can't not be empty.")]
        public string TIN { get; set; }

        
        public string TinProof { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string TAN { get; set; }

        public string TANProof { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string PAN { get; set; }

        public string PanProof { get; set; }


        [Required(ErrorMessage = "Can't not be empty.")]
        public string IDENTITY { get; set; }

        public string IDENTITYProof { get; set; }

        //VendorBankDetail
        [Required(ErrorMessage = "Can't not be empty.")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string BankAccountNumber { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string IFSCCode { get; set; }

        public string Awiftcode { get; set; }

        public string AchoderName { get; set; }

        public string UploadAddressProof { get; set; }
        public string UploadCancelledCheque { get; set; }

        public int VendorRegID { get; set; }
    

        
    }





}
