﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace MyShop
{
    //After add this field it will confirm email by your email account.
   public class Vendor
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Can't not be empty.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
         ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        //[RegularExpression(@"^([a-zA-Z0-9]{6,40}", ErrorMessage = "Password minimum 6 digit")]
        public string password { get; set; }

        public string Emailcode { get; set; }


        //[RegularExpression(@"^([0-9]{10}", ErrorMessage = "Only 10 digit Mobile no.")]
        public string ContactNo { get; set; }

        public string Mobilecode { get; set; }

        public string Emailverfiy { get; set; }
        public string Contactvcerfiy { get; set; }
        public string Singupverfiy { get; set; }

        public bool IsActive { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedOnUtc { get; set; }


        [DataType(DataType.Date)]
        public DateTime UpdatedOnUtc { get; set; }
        public bool isbestseller { get; set; }
       
    }
}
