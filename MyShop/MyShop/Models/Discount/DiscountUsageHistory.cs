﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using G2MFashions.Models.Order;
namespace G2MFashions.Models.Discount
{
    public class DiscountUsageHistory
    {
        public int ID { get; set; }

        [Required]
        public int DiscountID { get; set; }
        public virtual Discount Discount { get; set; }

        [Required]
        public int CustomerOrderID { get; set; }
        public virtual CustomerOrder CustomerOrder { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreatedOnUtc { get; set; }
    }
}