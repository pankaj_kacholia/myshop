﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MyShop;
namespace MyShop
{
    public class Discount_AppliedToProducts
    {
        public int ID { get; set; }

        //[Required]
        //public int DiscountID { get; set; }
        //public virtual Discount Discount { get; set; }


        [Required]
        public int ProductID { get; set; }
        public virtual Product Product { get; set; }
    }
}