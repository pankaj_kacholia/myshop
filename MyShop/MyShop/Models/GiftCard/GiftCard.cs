﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Models.GiftCard
{
    public class GiftCard
    {
        public int ID { get; set; }
       [Required(ErrorMessage = "*")]
        public string GiftCardName { get; set; }

       public string GiftCardDescription { get; set; }

        public string GiftcardPic { get; set; }
        [Required(ErrorMessage = "*")]
        public decimal Price { get; set; }

        public DateTime GiftCardAddDate { get; set; }

        public DateTime GiftCardUpdateDate { get; set; }

    }
}
