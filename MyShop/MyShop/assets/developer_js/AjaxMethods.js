﻿
function AjaxCallForPost(url, data, loader, successCallback) {


    if (loader == true)
        $(".loader").show();

    console.log("data=" + data + "url=" + url, +"loader=" + loader);
    //  alert("in");

    $.ajax({
        type: "Post",
        data: data,
        url: url,
        success: function (data) {
            successCallback(data);
            $(".loader").hide();
            //   alert("sucess");
        },
        error: function (data) {
            alert("Some unexpected error occurred");
            $(".loader").hide();

        }
    });
}

function AjaxCallForGet(url, data, loader, successCallback) {


    if (loader == true)
        $(".loader").show();

    console.log("url=" + url, +"loader=" + loader,+"data=",data);
    //  alert("in");

    $.ajax({
        type: "get",
        url: url,
        data: data,
        success: function (data) {
            console.log(data);
            successCallback(data);
            $(".loader").hide();
            //   alert("sucess");

        },
        error: function (data) {
            alert("Some unexpected error occurred");
            $(".loader").hide();

        }
    });
}

function ImageFileSend(url, data, loader, successCallback) {

    $("#uploadEditorImage").change(function () {
        var data = new FormData();
        var files = $("#uploadEditorImage").get(0).files;
        console.log("files", files);                                                                
        if (files.length > 0) {
            data.append("HelpSectionImages", files[0]);
        }
        $.ajax({
            url: url,
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                //code after success

            },
            error: function (er) {
                alert(er);
            }

        });
    });
}