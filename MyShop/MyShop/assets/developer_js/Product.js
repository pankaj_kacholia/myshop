﻿function CreatePage() {


    AjaxCallForGet("/Products/Create","",true, function (data) {

        $(".panel-body").show();
        $("#showpage").html(data);
        $("#indexpage").hide();
    })

}


function Create() {

    var data = $("#product_form").serialize();
    console.log(data);
    var formdata = new FormData(); //FormData object
    var fileInput = document.getElementById('fileInput');
    //Iterating through each files selected in fileInput
    for (i = 0; i < fileInput.files.length; i++) {
        //Appending each file to FormData object
        formdata.append(fileInput.files[i].name, fileInput.files[i]);
    }
    ImageFileSend("/Products/Create", data, true, function (data) {
        //  CallMethod()
        $(".panel-body").show();
        $(".message").children().html(data.message);
    })
}

function EditData(id) {

    AjaxCallForGet("/Products/EditCategories", { id: id }, true, function (data) {
        alert(data);
        //CallMethod()
        $(".panel-body").show();
        $("#showpage").html(data);
        //  $(".message").children().html(data.message);
    })

}


function Update() {

    var data = $("#role_form").serialize();
    AjaxCallForPost("/Products/EditCategories", data, true, function (data) {
        //CallMethod()

        $(".panel-body").show();
        $(".message").children().html(data.message);
    })

}
function CallMethod() {
    $(".panel-body").show();
    $(".message").children().html(data.message);
}