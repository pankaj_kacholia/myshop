﻿var status = "showpage";    
var messageStatus = "show_message";

function CreatePage(url) {
    AjaxCallForGet(url, "", true, function (data) {       
        HideIndex(status, data);
    })
}

function Create(url,formName) {
    var data = $(formName).serialize();
    console.log(data);
    ImageFileSend(url, data, true, function (data) {
        HideIndex(status, data)
    })
}

function EditData(url, id) {   
    AjaxCallForGet(url, { id: id }, true, function (data) {        
        HideIndex(status, data)      
    })

}

function Update(url,formName) {
    var data = $(formName).serialize();
    AjaxCallForPost(url, data, true, function (data) {
        HideIndex(messageStatus, data)
    })
}

function HideIndex(showStatus, data) {
    if (showStatus == messageStatus) {
        $(".panel-body").show();
        $(".message").children().html(data.message);
    }
    else {
        $(".panel-body").show();
        $("#indexpage").hide();
        if (showStatus == status) {
            $("#showpage").html(data);
        }
    }
}